package org.cea.lise.security.watermarking.bch;

public interface BCHFactory {
	
	public BCHdecoder createBCHdecoder();

}
