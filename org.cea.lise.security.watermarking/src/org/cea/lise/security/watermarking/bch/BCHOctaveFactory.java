package org.cea.lise.security.watermarking.bch;

public class BCHOctaveFactory implements BCHFactory{

	@Override
	public BCHdecoder createBCHdecoder() {
		return new OctaveBCH();
	}

}
