package org.cea.lise.security.watermarking.bch;

public abstract class BCHdecoder {
	
	public abstract Integer[] decode(int k, int t, int[] bytes);

}
