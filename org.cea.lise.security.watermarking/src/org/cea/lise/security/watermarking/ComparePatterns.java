package org.cea.lise.security.watermarking;

import java.io.File;
import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.cea.lise.security.watermarkin.image.ImageUtil;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

public class ComparePatterns{
	
	private static Random generator = new Random(12345);
	//We fix a gap of 4 for the experiment
	private static int gap = 8;

	public static void main(String[] args) {
		
		//We store here the masks obtained for each metamodel
		HashMap<String, byte []> masksMap = new HashMap<String, byte[]>();
		
		List <String> models = getModelsList("Z:\\git\\mde-watermarking\\org.cea.lise.security.watermarking\\metamodel");
		System.out.println(models.toString());
		
		BigInteger b = new BigInteger("967898237293942389994949283205239421123354199393930343234999");
		String s = b.toString(2);
		byte[] input = getBits(s);
		
		System.out.println("size is " + input.length);
		
		ZeroWatermark zw = new ZeroWatermark();
		LabelModel lm = new LabelModel();
		
		EObject root = ModelUtil.doLoadXMIFromPath("metamodel" + File.separator + "ProMarte.ecore");
		System.out.println(numClasses(root));
		
		double bitsPerEObject = ((double)((double)input.length / (double)numClasses(root)) * (double)gap);
		
		
		
		System.out.println((int)bitsPerEObject);
		
		byte [] mask2 = zw.insert(root, (int)bitsPerEObject, input);
		
		for (String metamodel: models) {
			System.out.println(metamodel);
			root = ModelUtil.doLoadXMIFromPath("metamodel" + File.separator + metamodel);
			int numClasses = numClasses(root);
			System.out.println(numClasses);
			bitsPerEObject = ((double)((double)input.length / (double)numClasses(root)) * (double)gap);
			System.out.println((int)bitsPerEObject);
			//lm.writeLabels(root);
			mask2 = zw.insert(root, (int)bitsPerEObject, input);
			masksMap.put(metamodel, mask2);
		}
		masksMap.get("ATL.ecore");
		double [][] similarities = calculateSimilarities(masksMap);
		
		System.out.println(Arrays.deepToString(similarities));

	}
	
	public static double [][] calculateSimilarities(HashMap<String, byte []> masksMap) {
		double [][] result = new double[masksMap.size()][masksMap.size()];
		Iterator it = masksMap.entrySet().iterator();
		int i = 0;
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			byte [] sourceMask = (byte[]) pair.getValue();
			Iterator targetIt = masksMap.entrySet().iterator();
			int j = 0;
			while (targetIt.hasNext()) {
				Map.Entry targetPair = (Map.Entry)targetIt.next();
				byte [] targetMask = (byte[]) targetPair.getValue();
				Double val = ImageUtil.simplematchingCoefficient(sourceMask, targetMask);
				result[i][j] = val;
				if (val != 1.0) {
					System.out.println( (String)pair.getKey() + "-" + (String)targetPair.getKey() + ":" + val*100);
					//System.out.println(val*100);
				}
				j++;
			}
			i++;
		}
		return  result;
	}
	
	/**
	 * List files in a Folder
	 * @param path
	 * @return
	 */
	public static List<String> getModelsList(String path){
		List<String> results = new ArrayList<String>();
		File[] files = new File(path).listFiles();
		//If this pathname does not denote a directory, then listFiles() returns null. 

		for (File file : files) {
			if (file.isFile()) {
				results.add(file.getName());
			}
		}
		return results;
	}
	
	/**
	 * Transform a string containing "1' and '0' into a byte array
	 * @param source
	 * @return
	 */
	public static byte [] getBits(String source) {
		byte [] result = new byte[source.length()];
		for (int i = 0; i < source.length(); i++) {
			if (source.charAt(i) == '0') {
				result[i] = 0;
			}else {
				result[i] = 1;
			}
		}
		return result;
	}
	
	/**
	 * Counts the number of metaclasses in a metamodel. There is probably some better way of doing this
	 * @param root
	 * @return
	 */
	public static int numClasses(EObject root) {
		int total = 0;
		TreeIterator<EObject> contents = root.eAllContents();
		while (contents.hasNext()) {
			EObject next = contents.next();
			if (next instanceof EClass) {
				total++;
			}
		}
		return total;
	}

}
