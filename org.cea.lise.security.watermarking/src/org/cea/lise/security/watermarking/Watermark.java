package org.cea.lise.security.watermarking;


import org.eclipse.emf.ecore.EObject;

public interface Watermark {
	
	/**
	 * This method supposes that the watermark unit is the model element. It basically traverses all the
	 * model while calling the mark() operation. 
	 * @param root
	 * @return
	 */
	public byte[] insert(EObject root, int numBits, byte [] message);
	
	public byte [] mark(EObject input, int numBits);
	
	public byte [] extract();
	
	public int threshold(int watermarkedElements, int foundMarks);

}
