package org.cea.lise.security.watermarking;

import java.io.File;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

public class LabelComparator {
	static int totalMutations = 0;
	public static void main(String[] args) {
		
		EObject root1 = ModelUtil.doLoadXMIFromPath("model" + File.separator + "ATL2.ecore");
		EObject root2 = ModelUtil.doLoadXMIFromPath("model" + File.separator + "ATL2_25.ecore");
		
		//how do we know it is the same element?
		HashMap<String, EObject> hm1 = getElements(root1);
		HashMap<String, EObject> hm2 = getElements(root2);
		
		// Get a set of the entries
	    Set<Entry<String, EObject>> set = hm1.entrySet();
	    Iterator<Entry<String, EObject>> i = set.iterator();
	   
	    // Display elements
	    int survivedLabels = 0;
	    while(i.hasNext()) {
	       Map.Entry me = (Map.Entry)i.next();
	       EObject e1 = (EObject) me.getValue();
	       EObject e2 = hm2.get(me.getKey());
	       
	       survivedLabels += compareLabels(e1, e2);
	       
	       //System.out.print(me.getKey() + ": ");
	       //System.out.println(me.getValue());
	    }
	    
	    System.out.println("TotalMutations: " + totalMutations);
	    System.out.println("Survived Labels: " + survivedLabels);
	    System.out.println("Percentage: " + (survivedLabels*100)/totalMutations);
	}
	
	public static Set<String> getSummary(EObject e){
		String summary = EcoreUtil.getAnnotation((EModelElement) e, e.eResource().getURI().toString(), "summary");
		summary = summary.replace("[", "");
		summary = summary.replace("]", "");
		String[] values = summary.split(",");
        return new HashSet<String>(Arrays.asList(values));
	}
	
	public static String getLabel(EObject e){
		return EcoreUtil.getAnnotation((EModelElement) e, e.eResource().getURI().toString(), "labelint");
	}
	
	/**
	 * Creates a hashmaps with all EObjects in a model indexed by their name
	 * @param root
	 * @return
	 */
	public static HashMap<String, EObject> getElements(EObject root) {
		HashMap<String, EObject> hm = new HashMap<String, EObject>();
		
		TreeIterator<EObject> contents = root.eAllContents();
		while (contents.hasNext()) {
		      EObject next = contents.next();	   
		      if (next instanceof EClass) {
		    	  EClass ec = (EClass) next;
		    	  hm.put(ec.getName(), next);
		      }
		}
		return hm;
		
	}
	
	public static Set<String> union(Set<String> s1, Set<String> s2){
		Set<String> union = new HashSet<String>(s1);
		union.addAll(s2);
		return union;
	}
	
	public static Set<String> intersection(Set<String> s1, Set<String> s2){
		Set<String> intersection = new HashSet<String>(s1);
		intersection.retainAll(s2);
		return intersection;
	}
	
	public static Set<String> difference(Set<String> s1, Set<String> s2){
		Set<String> difference = new HashSet<String>(s1);
		difference.removeAll(s2);
		return difference;
	}
	
	public static int getMutationsNumber(Set<String> s1, Set<String> s2) {
		return difference(s2, s1).size();
	}
	
	public static int compareLabels(EObject e1, EObject e2) {
		Set<String> summary1 = getSummary(e1);
	    Set<String> summary2 = getSummary(e2);
	    int numMutations = getMutationsNumber(summary1, summary2);
	    if (numMutations > 0) {
	    	if (e1 instanceof EClass) {
		         EClass c = (EClass) e1;	
		         System.out.println(c.getName() + " Number of mutations = " + numMutations);
		         totalMutations++;
		         if (getLabel(e1).equalsIgnoreCase(getLabel(e2))) {
		        	 System.out.println("Label Survived");
		        	 return 1;
		         }
	    	}
	    }
		return 0;
	}

}
