package org.cea.lise.security.watermarking.minhash;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Test {

	public static void main(String[] args) {
		MinHash minh = new MinHash(20);
		
		Set<String> s = new HashSet<String>();
		s.add("this");
		s.add("is");
		s.add("an");
		s.add("example");
		s.add("of");
		s.add("seto");
		s.add("for");
		s.add("hash");
		s.add("forminhash");
		
		int[] signature = minh.minHashSig(s);

		System.out.println(Arrays.toString(signature));
		
		
		Set<String> s2 = new HashSet<String>();
		s2.add("this");
		s2.add("is");
		s2.add("an");
		s2.add("example");
		s2.add("of");
		s2.add("set");
		s2.add("for");
		s2.add("hash");
		s2.add("forminhash");
		
		int[] signature2 = minh.minHashSig(s2);

		System.out.println(Arrays.toString(signature2));
		
		
		System.out.println(minh.approximateJaccard(signature, signature2));

	}

}
