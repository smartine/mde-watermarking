package org.cea.lise.security.watermarking.minhash;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Generates Minhash for sets of strings.  
 * 
 * The permutations are represented by randomized hash functions: ax + b % p.
 * p is a prime such that p >= n where n is the number of terms in the collection.
 * a and b are chosen uniformly at random from {1,2,...,p-1}.
 *
 */
public class MinHash {
	int numPermutations;
	//int numTerms;
	int mod; //p: ax + b % p
	List<Pair> AB; //a, b: ax + b % p

	/**
	 * Constructor that takes a folder and number of permutations.
	 * @param numPermutations Number of permutations for MinHash.
	 */
	public MinHash(int numPermutations) {
		this.numPermutations = numPermutations;
		
		//numTerms = 20;
		BigInteger bi = BigInteger.valueOf(numPermutations);
		mod = Integer.parseInt(bi.nextProbablePrime().toString());
		AB = generateCoefficients(mod);
	}
	
	/**
	 * Calculates the MinHash signature.
	 * @param summary is a string set to be hashed
	 * @return MinHash signature as int array.
	 */
	public int[] minHashSig(Set<String> summary) {
		int hashVal;
		int[] minHashVals = new int[numPermutations];
		Arrays.fill(minHashVals, Integer.MAX_VALUE);
		for (String s : summary) {
			for(int i = 0; i < numPermutations; i++) { //hash through k-functions
				hashVal = word2int(s, AB.get(i).a, AB.get(i).b, mod);
				if(hashVal < minHashVals[i]){
					minHashVals[i] = hashVal;
				}
			}
		}
		return(minHashVals);
	}
	
	/**
	 * Computes the approximate jaccard simularity by using the MinHash signatures.
	 * @param d1 MinHash signature of first document.
	 * @param d2 MinHash signature of second document.
	 * @return Approximate jaccard simularity.
	 */
	public double approximateJaccard(int[] d1, int[] d2) {
		double numMatch = 0.0;
		for(int i = 0; i < numPermutations; i++) {
			if(d1[i] == d2[i]) numMatch++;
		}
		
		return(numMatch / numPermutations);
	}
	
	
	/**
	 * Gives the number of permutations used for MinHash matrix.
	 * @return Number of permutations
	 */
	public int numPermutations() {
		return(numPermutations);
	}
	
	/**
	 * Hashes a word into an integer using ax + b % p hash function.
	 * @param s Word to hash.
	 * @param a First coefficient in hash function.
	 * @param b Second coefficient in hash function.
	 * @param mod Modulus of hash function.
	 * @return Hash value of word.
	 */
	private int word2int(String s, int a, int b, int mod) {
		int hashed = 0;
		
		for(int i = 0; i < s.length(); i++) {
			hashed ^= s.charAt(i);
			hashed = a + b * hashed;
			hashed = hashed % mod;
		}
		
		return(hashed);
	}
	
	/**
	 * Container object for a pair of coefficients to be used as part of the hash function.
	 * Hash functions of form ax + b % p, this object will store coefficients a and b.
	 * 
	 * This is mainly used as a quick way to check if any of the k-hash functions used for 
	 * the MinHash matrix are duplicated.
	 * @author Alex Shum
	 */
	public class Pair {
		int a, b;
		
		/**
		 * Creates a new coefficient pair container.
		 * @param a The first coefficient.
		 * @param b The second coefficient.
		 */
		public Pair(int a, int b) {
			this.a = a;
			this.b = b;
		}
		
		/**
		 * Checks if another pair container is equal to this one.
		 * @param other The other pair to check for equality.
		 * @return true if both coefficients are equal.  Otherwise false.
		 */
		@Override
		public boolean equals(Object other) {
			if(other == null) return(false);
			if(other == this) return(true);
			if(!(other instanceof Pair)) return(false);
			
			Pair p = (Pair) other;
			return(a == p.a && b == p.b);
		}
	}
	
	/**
	 * Generates k-random hash functions.  k is equal to the number of permutations.
	 * @param mod The modulus for the hash function.
	 * @return List of pairs of coefficients for hash functions.
	 */
	private List<Pair> generateCoefficients(int mod) {
		Random r = new Random(34829304);
		List<Pair> coef = new ArrayList<Pair>();
		
		Pair p = new Pair(r.nextInt(mod), r.nextInt(mod));
		for(int i = 0; i < numPermutations; i++) {
				p = new Pair(r.nextInt(mod), r.nextInt(mod));
			coef.add(p);
		}
		
		return(coef);
	}
	
}