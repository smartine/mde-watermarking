package org.cea.lise.security.watermarking;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public class ModelUtil {
	
public static EObject doLoadXMIFromPath(String Path) {
        
        Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
        Map<String, Object> m = reg.getExtensionToFactoryMap();
        m.put("ecore", new XMIResourceFactoryImpl());

        ResourceSet resSet = new ResourceSetImpl();
        Resource resource = resSet.getResource(URI.createURI(Path), true);
        try {
			resource.load(m);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
        //
        //FIXME:  we suppose there is a types package so we skip it and take second package. 
        //
        return resource.getContents().get(0);
       
	}

public static Set<String> getSummary(EObject e){
	String summary = EcoreUtil.getAnnotation((EModelElement) e, e.eResource().getURI().toString(), "summary");
	summary = summary.replace("[", "");
	summary = summary.replace("]", "");
	String[] values = summary.split(",");
    return new HashSet<String>(Arrays.asList(values));
}

public static String getLabel(EObject e){
	return EcoreUtil.getAnnotation((EModelElement) e, e.eResource().getURI().toString(), "labelint");
}

}
