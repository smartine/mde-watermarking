package org.cea.lise.security.watermarking;


import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Random;
import java.util.Set;

import javax.imageio.ImageIO;

import org.cea.lise.security.watermarkin.image.ImageUtil;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

public class ZeroWatermark implements Watermark{
	
	/**
	 * Our randomizer. It should be seeded with the private key of the user
	 */
	private static Random generator = new Random(12345);
	private static int gap = 16;
	
	public static void main(String[] args) {
		
		//BufferedImage im = ImageUtil.openImage("images/gamepad2.bmp");
		//byte[][] image = ImageUtil.byteBinaryImage(im);
		//byte [] flattenedImage = ImageUtil.flattenImage(image, 32, 32); 
		//ImageUtil.writeBinaryImage("images/gamepad2b.bmp", ImageUtil.createImageFromBytes(32, 32, flattenedImage));
		
		
		BigInteger b = new BigInteger("967898237293942389994949283205239421123354199393930343234999");
		String s = b.toString(2);
		byte[] input = getBits(s);
		
		System.out.println("size is " + input.length);
		
		ZeroWatermark zw = new ZeroWatermark();
		
		int numBits = 200;
		int bitsPerEObject = 26; 
		//// numBits/(240/gap);
		EObject root = ModelUtil.doLoadXMIFromPath("model" + File.separator + "ProMarte.ecore");
		
		generator = new Random(12345);
		//input is passed just to get the lengt...
		byte [] mask = zw.insert(root, bitsPerEObject, input);
		//We get the key here
		byte [] resultKey = zw.getXorKey(input, mask);
		
		//byte [] obtainedImage = zw.getXorKey(resultKey, mask);
		
		
		//Now... we try to get the same from a modified model. Same steps.
		
		root = ModelUtil.doLoadXMIFromPath("model" + File.separator + "UML2_75.ecore");
		
		generator = new Random(12345);
		byte [] mask2 = zw.insert(root, bitsPerEObject, input);
		//if we xor this second mask with the key, we should get our image back, with some distortion
		byte [] obtainedPattern = zw.getXorKey(resultKey, mask2);
		
		if (Arrays.equals(mask, mask2)) {
			System.out.println("Youpi");
		}else {
			System.out.println("Nein");
		}
		
		Double result = ImageUtil.simplematchingCoefficient(input, obtainedPattern);
		System.out.println(result);
		Double numFound = ImageUtil.numFound(input, obtainedPattern);
		System.out.println(numFound);
		
	}

	@Override
	public byte [] insert(EObject root, int numBits, byte[] message) {
		int readbits = 0;
		int messageLenght = message.length;
		int remaining = messageLenght;
		byte [] modelBits = new byte[messageLenght];
		TreeIterator<EObject> contents = root.eAllContents();
		while (contents.hasNext() && readbits < messageLenght) {
			remaining = messageLenght - readbits;
			int toRead = (remaining) >= numBits ? numBits : (remaining);
			EObject next = contents.next();
			if (next instanceof EClass) { 
		      //test if elegible by getting the label
		      BigInteger bi = new BigInteger(ModelUtil.getLabel(next));
		      if (!bi.mod(new BigInteger(Integer.toString(gap))).equals(BigInteger.ZERO)) {
		    	  byte [] part = mark(next, toRead);
		    	  for (int i = 0; i < toRead; i++) {
		    		  modelBits[readbits + i] = part[i]; 
		    	  }
		    	  readbits += part.length;
		      }
			}
		}
		return modelBits;
	}

	@Override
	public byte[] mark(EObject input, int numBits) {
		//We work with the summary of the EObject. 
		int readBits = 0;
		Set<String> s = ModelUtil.getSummary(input);
		String [] summary = (String[]) s.toArray(new String [s.size()]);
		String binary = "";
		while (binary.length() < numBits){
			String val = summary[generator.nextInt(summary.length)];
			//String halfVal = val.substring(0, (val.length()));
			binary += new BigInteger(val.getBytes()).toString(2);
			readBits += binary.length();
		}
		
		if (binary.length() > numBits) {
			byte [] part = getBits(binary);
			return Arrays.copyOf(part, numBits);
		}
		return getBits(binary);
		
	}

	@Override
	public byte[] extract() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int threshold(int watermarkedElements, int foundMarks) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public static byte [] concat(byte[] modelbytes, byte[] part) {
		  byte [] result = Arrays.copyOf(modelbytes, modelbytes.length + part.length);
		  System.arraycopy(part, 0, result, modelbytes.length, part.length);
		  return result;
		}
	
	public static byte [] getBits(String source) {
		byte [] result = new byte[source.length()];
		for (int i = 0; i < source.length(); i++) {
			if (source.charAt(i) == '0') {
				result[i] = 0;
			}else {
				result[i] = 1;
			}
		}
		return result;
	}
	
	public byte[] getXorKey (byte[] entry, byte [] mask) {
		byte [] key = new byte[entry.length];
		int i = 0;
		for (byte b : entry)
		    key[i] = (byte) (b ^ mask[i++]);
		return key;
	}

}
