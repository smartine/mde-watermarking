package org.cea.lise.security.watermarking;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import org.cea.lise.security.watermarking.bch.BCHFactory;
import org.cea.lise.security.watermarking.bch.BCHOctaveFactory;
import org.cea.lise.security.watermarking.bch.BCHdecoder;
import org.cea.lise.security.watermarking.minhash.MinHash;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.impl.EReferenceImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import bchcodes.CodeBCH;
import bchcodes.Polynome;

public class LabelModel {
	
	static int minhashk = 15;
	static int bchk = 15;
	static int t = 3;
	static int wordSize = 5;
	static MinHash minh = new MinHash(minhashk);
	static BCHFactory bchfactory = new BCHOctaveFactory();
	static BCHdecoder bchdecoder = bchfactory.createBCHdecoder();

	public static void main(String[] args) {
		
		LabelModel lm = new LabelModel();
		
		
		EObject root = ModelUtil.doLoadXMIFromPath("model" + File.separator + "ATLX.ecore");
		
		
		TreeIterator<EObject> contents = root.eAllContents();
		while (contents.hasNext()) {
		      EObject next = contents.next();
		      if (next instanceof EClass) {
		         EClass c = (EClass) next;
		         String sourceAnnotationUri = c.eResource().getURI().toString();
		         Set<String> s = lm.calculateSummary(c);
		         EcoreUtil.setAnnotation(c, sourceAnnotationUri, "summary" , s.toString());
		         int [] signature = minh.minHashSig(s);
		         Integer [] labelDecoded = lm.decodeLabel(signature, bchdecoder);
		         String label = lm.bytes2bits(labelDecoded);
		         EcoreUtil.setAnnotation(c, sourceAnnotationUri, "signature" , Arrays.toString(signature));
		         EcoreUtil.setAnnotation(c, sourceAnnotationUri, "label" , Arrays.toString(labelDecoded));
		         EcoreUtil.setAnnotation(c, sourceAnnotationUri, "labelBits" , label);
		         BigInteger labelint = new BigInteger(label, 2);
		         EcoreUtil.setAnnotation(c, sourceAnnotationUri, "labelint" , labelint.toString());
		         

		         //To get the annotations
		         //String[] values = csv.split(",");
		         //Set<String> hashSet = new HashSet<String>(Arrays.asList(values));


		      }
		}
			 
		
		try {
			root.eResource().save(null);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public void writeLabels(EObject root) {
		TreeIterator<EObject> contents = root.eAllContents();
		while (contents.hasNext()) {
		      EObject next = contents.next();
		      if (next instanceof EClass) {
		         EClass c = (EClass) next;
		         String sourceAnnotationUri = c.eResource().getURI().toString();
		         Set<String> s = calculateSummary(c);
		         EcoreUtil.setAnnotation(c, sourceAnnotationUri, "summary" , s.toString());
		         int [] signature = minh.minHashSig(s);
		         Integer [] labelDecoded = decodeLabel(signature, bchdecoder);
		         String label = bytes2bits(labelDecoded);
		         EcoreUtil.setAnnotation(c, sourceAnnotationUri, "signature" , Arrays.toString(signature));
		         EcoreUtil.setAnnotation(c, sourceAnnotationUri, "label" , Arrays.toString(labelDecoded));
		         EcoreUtil.setAnnotation(c, sourceAnnotationUri, "labelBits" , label);
		         BigInteger labelint = new BigInteger(label, 2);
		         EcoreUtil.setAnnotation(c, sourceAnnotationUri, "labelint" , labelint.toString());
		         
		      }
		}
			 
		
		try {
			root.eResource().save(null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Set<String> calculateSummary(EClass c){
		Set<String> set = new HashSet<String>();

		//We add the name to the summary
		set.add(c.getName());
		
		//We are going to add extra feature of the element to enrich the set. This would make it
		//more resilient to changes.
		
		
		if (c.isAbstract())
			set.add("ThisIsAnAbstractClass");
		else
			set.add("ThisIsNotAnAbstractClass");
		
		if (c.isInterface())
			set.add("ThisIsAnInterfaceClass");
		else
			set.add("ThisIsARegularClassNonInterface");
		

		//We add the super types to the summary
		for (EClass item : c.getESuperTypes()) {
			set.add(item.getName());
		}

		//We add the container to the summary
		set.add(c.eContainer().eClass().getName());


		//We add the attributes
		for (EAttribute att : c.getEAttributes()) {
			set.add(att.getName());
		}

		//We add all the references
		for (EReference ref : c.getEReferences()) {
			set.add(ref.getName());
		}

		//We add all the operations
		for (EOperation opr : c.getEOperations()) {
			set.add(opr.getName());
		}

		//We add all the crossReferences		     		         
		Collection<Setting> references = EcoreUtil.UsageCrossReferencer.find(c, c.eResource());
		for (Setting setting : references) {
			EObject source = setting.getEObject();
			if (source instanceof EReferenceImpl) {
				if (source.eContainer() instanceof EClass) {
					EClass ec = (EClass) source.eContainer();
					System.out.println(">" + ec.getName());
					set.add(ec.getName());
				}
			}
		}
		
		return set;	 
	}

	
	public Integer [] decodeLabel(int[] input, BCHdecoder decoder) {
		
		
		
		int []  input1 = Arrays.copyOfRange(input, 0, bchk);
		
		return decoder.decode(wordSize, t, input);
//		int []  input2 = Arrays.copyOfRange(input, bchk, input.length);
//		
//		Integer [] decoded = Stream.concat(Arrays.stream(decoder.decode(wordSize, t, input1)), Arrays.stream(decoder.decode(wordSize, t, input2)))
//                .toArray(Integer[]::new);
//		
//		return decoded;
	}
		
		
	public String bytes2bits(Integer[] input) {
		String binaryString = "";
		for (int i : input) {
			binaryString = binaryString.concat(this.pad(Integer.toBinaryString(i),5));
		}
		return binaryString;
	}
	
	
	private static List<String> getParts(String string, int partitionSize) {
        List<String> parts = new ArrayList<String>();
        int len = string.length();
        for (int i=0; i<len; i+=partitionSize)
        {
            parts.add(string.substring(i, Math.min(len, i + partitionSize)));
        }
        return parts;
    }
	
	private static String addCeros(String input, int size) {
		String output = input;
		for (int i=0; i < size - input.length(); i++) {
			output = output.concat("0");
		}
		return output;
	}
	
	public String pad(String s, int numDigits)
    {
        StringBuffer sb = new StringBuffer(s);
        int numZeros = numDigits - s.length();
        while(numZeros-- > 0) { 
            sb.insert(0, "0");
        }
        return sb.toString();
    }

}
