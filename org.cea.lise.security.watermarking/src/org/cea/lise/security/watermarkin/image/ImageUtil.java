package org.cea.lise.security.watermarkin.image;

import java.awt.Color;
import java.awt.Point;
import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


public class ImageUtil {

	public static void main(String[] args) {
		BufferedImage im = openImage("images/gamepad2.bmp");
		byte[][] image = byteBinaryImage(im);
		image.toString();
		System.out.println(image);
		
		byte [] flattenedImage = flattenImage(image, 32, 32); 
		
		System.out.println(image);
		
		writeBinaryImage("images/black32.bmp", im);

	}
	
	public static BufferedImage openImage(String path) {
		BufferedImage image = null;
		try {
			image = ImageIO.read(new File(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return image; 
	}
	
	public static byte[][] byteBinaryImage(BufferedImage image){


		byte[][] pixels = new byte[image.getWidth()][];

		for (int x = 0; x < image.getWidth(); x++) {
			pixels[x] = new byte[image.getHeight()];

			for (int y = 0; y < image.getHeight(); y++) {
				int rgb = image.getRGB(x, y);
				Color mycolor = new Color(image.getRGB(x, y));
				if (mycolor.equals(Color.BLACK)) {
					pixels[x][y] = 1;
				}else {
					pixels[x][y] = 0;
				}
				//pixels[x][y] = (byte) (image.getRGB(x, y) == 0xFFFFFFFF ? 0 : 1);
				//black 0xff000000
			}
		}
		return pixels;


	}
	
	public static void writeBinaryImage(String path, BufferedImage image){
		try {
//			for (int x = 0; x < image.getWidth(); x++) {
//				
//
//				for (int y = 0; y < image.getHeight(); y++) {
//					
//					image.setRGB(x, y, 0xFF000000);
//				}
//			}
			ImageIO.write(image, "bmp", new File(path));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static byte[] flattenImage(byte[][] image, int rows, int columns) {
		byte [] result = new byte[rows*columns];
		int index = 0;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				result[index] = image[i][j];
				index++;
			}
		}
		return result;
	}
	
	public static double simplematchingCoefficient(byte[] a, byte[] b) {
		
		double m00 = 0;
		double m01 = 0;
		double m10 = 0;
		double m11 = 0;
		
		for (int i = 0; i < a.length; i++) {
			if (a[i] == 1 && b[i] == 1 ) {
				m00++;
			}else if (a[i] == 0 && b[i] == 1) {
				m01++;
			}else if (a[i] == 1 && b[i] == 0) {
				m10++;
			}else {
				m11++;
			}
			
		}
		
		return (m00 + m11) / (m00 + m01 + m10 + m11);
	}
	
public static double numFound(byte[] a, byte[] b) {
		
		double m00 = 0;
		double m01 = 0;
		double m10 = 0;
		double m11 = 0;
		
		for (int i = 0; i < a.length; i++) {
			if (a[i] == 1 && b[i] == 1 ) {
				m00++;
			}else if (a[i] == 0 && b[i] == 1) {
				m01++;
			}else if (a[i] == 1 && b[i] == 0) {
				m10++;
			}else {
				m11++;
			}
			
		}
		
		return (m00 + m11);
	}
	
	public static BufferedImage createImageFromBytes(int width, int height, byte[] imageData) {
		
		BufferedImage img = openImage("images/black32.bmp");
	    
		int imageDataIndex = 0;
		for (int x = 0; x < img.getWidth(); x++) {
			for (int y = 0; y < img.getHeight(); y++) {
				if (imageData[imageDataIndex] == 0){
					img.setRGB(x, y, Color.white.getRGB());
				}else {
					img.setRGB(x, y, Color.black.getRGB());
				}
				imageDataIndex++;
			}
		}
		return img;
	}

}
