package bchcodes;
import java.util.Vector;


/**
 * Corps fini Fq  (q = p^m).
 **/
public class Corps {

    /**
     * Dimension du corps.
     */
    int q;

    /**
     * Dimension de l'espace constructeur Fp. Doit �tre un nombre premier.
     */
    int p;

    int m;   

    /**
     * Polynome irreductible (de degr� m).
     */
    Polynome irreductible;   

    /**
     * Elements primitifs.
     */
    Polynome[] primitifs;   

    /**
     * Table des logarithmes.
     */
    Polynome[] tableLogs;


    /**
     * Construit un corps fini Fq (q = p^m) avec un polyn�me irreductible
     * comme g�n�rat�ur.
     * @param p  un entier premier (la base)
     * @param m  un entier (le degr� du polyn�me irreductible)
     * @param irreductible  un polyn�me irreductible de degr� m qui engendrera
     * le corps
     */    
    public Corps(int p, int m, Polynome irreductible) throws CorpsException {
	this.p = p;
	this.m = m;
	this.q = (int) Math.pow(p, m);

	if (!Main.estPremier(p)) { 
	    throw new CorpsException("Erreur: Impossible construire F" + 
				     q + " (" + p + "^" + m + "), " +
				     p + " n'est pas un nombre premier");
	}
	if (irreductible.degre() != m) {
	    throw new CorpsException("Erreur dans la construction de F" + 
				     q + " (" + p + "^" + m + "), " +
				     "le polyn�me irreductible devrait �tre " +
				     "de degr� " + m);
	}
	this.irreductible = new Polynome(adjusteCoeffsPolynome(irreductible)); 
	                                                     // pour s�curit�
	Polynome[] tousLesPolynomes = tousLesPolynomes();
	this.primitifs = recherchePrimitifs(tousLesPolynomes);
	this.tableLogs = tableLogs();

    }


    /**
     * Construit un corps fini Fq avec un polyn�me irreductible
     * comme g�n�rat�ur. Ce constructeur "l�ger" ne calcule pas les primitifs
     * et la table des logarithmes; utilisable pour Berlekamp.
     * @param p  un entier premier (la base)
     * @param irreductible  un polyn�me irreductible qui engendrera le corps
     */    
    public Corps(int p, Polynome irreductible) throws CorpsException {
	this.p = p;
	this.m = irreductible.degre();
	this.q = (int) Math.pow(p, irreductible.degre());

	if (!Main.estPremier(p)) { 
	    throw new CorpsException("Erreur: Impossible construire F" + 
				     q + " (" + p + "^" + m + "), " +
				     p + " n'est pas un nombre premier");
	}
	this.irreductible = new Polynome(adjusteCoeffsPolynome(irreductible)); 
	                                                     // pour s�curit�
	Polynome[] tousLesPolynomes = tousLesPolynomes();
	this.primitifs = null;
	this.tableLogs = null;

    }



/*** Operations definies � l'interieur de Fp (= Z/pZ), c.a.d. en base p ***/ 



    /**
     * Somme deux elements a et b de Fp.
     * @param a  un entier de Fp
     * @param b  un entier de Fp
     * @return  a + b dans Fp
     */
    int add(int a, int b) {
	if ((a + b) < 0) return a + b + p;    // cas ou' b < 0
	// a et b appartenants a' Fp, a - b est toujours < p
	else return (a + b) % p;
    }



    /**
     * Multiplie deux elements a et b dans Fp. 
     * @param a  un entier 
     * @param b  un entier
     * @return  a * b dans Fp
     */
    int mult(int a, int b) {
	return (a * b) % p;
    }



    /**
     * Affiche la table de l'addition de Fp. 
     */
    void afficheTableAdd() {
	int i, j;

	for (i = 0; i < p; i++) {
	    for (j = 0; j < p; j++) {
		System.out.print(add(i, j) + " ");
	    }
	    System.out.print("\n");
	}
    }
    


    /** 
     * Affiche la table de la multiplication de Fp. 
     */
    void afficheTableMult() {
	int i, j;

	for (i = 0; i < p; i++) {
	    for (j = 0; j < p; j++) {
		System.out.print(mult(i, j) + " ");
	    }
	    System.out.print("\n");
	}
    }



/*** Operations d�finies sur le corps Fq = Fp^m ***/  



    /**
     * Transforme un polyn�me de Z vers Fp^m.
     * @param polyZ  un polyn�me de Z
     * @return  le polyn�me polyZ dans Fp^m
     */
    public Polynome adjustePuissancesPolynome(Polynome polyZ) {
	Polynome[] polysFpm = divise(polyZ, irreductible);

	return (polysFpm[1]);
    }



    /** 
     * Substitue tous les coefficients d'un polyn�me
     * avec leurs homologues en Fp.
     * @param polyZ  un polyn�me de Z
     * @return  le polyn�me polyZ dans Fp^m
     */
    public Polynome adjusteCoeffsPolynome(Polynome polyZ) {
        Polynome polyFpm = new Polynome(polyZ);
 
        for (int i = 0; i <= polyFpm.degre(); i++) {
            while (polyFpm.coeff[i] < 0) polyFpm.coeff[i] += p;
            polyFpm.coeff[i] %= p;
        }

        return polyFpm;
    }



    /**
     * Adjuste la puissance d'un �l�ment primitif (cyclique par q-1).
     * @param exposant  un entier
     * @return  le modulo q-1 positif de exposant
     */
    public int adjuste(int exposant) {
	while (exposant < 0) exposant += (q-1);
	return exposant % (q-1);
    }

 

    /** 
     * Engendre tous les polynomes de Fq (de degre' < m, a' coeffs en Z/pZ).
     * @return  un array contenant tous les q polyn�mes possibles de Fq 
     */
    Polynome[] tousLesPolynomes() {
	Polynome[] tousPolys = new Polynome[q];
	for (int t = 0; t < q; t++) {
	    tousPolys[t] = new Polynome();
	}
	int n;
	int k = 0;
	int i, j;
	
	for (n = 0; n < q; n++) {
	    for (i = 0; i < m; i++) {
		tousPolys[k].coeff[i] = n % ((int) Math.pow(p, i+1));
		for (j = i - 1; j >= 0; j--) {
		    tousPolys[k].coeff[i] -= (tousPolys[k].coeff[j] / 
					      Math.pow(p, j));
		}
		tousPolys[k].coeff[i] /= Math.pow(p, i);
	    }
	    k++;
	}

	return tousPolys;
    }



    /** 
     * Affiches la table d'addition des polynomes en Fq.
     * @param tousPolys  tous les polyn�mes de Fq
     */ 
    public void afficheTableAddPolynomes(Polynome[] tousPolys) {
	Polynome element = new Polynome();
	int i, j;
	
	for (i = 0; i < q; i++) {
	    for (j = 0; j < q; j++) {
		element = tousPolys[i].add(tousPolys[j]);
		element = adjusteCoeffsPolynome(element);
		System.out.print(element.affiche(m - 1) + " ");
	    }
	    System.out.print("\n");
	}    
    }



    /**
     * Affiches la table de multiplication des polynomes en Fq. 
     * @param tousPolys  tous les polyn�mes de Fq
     */ 
    public void afficheTableMultPolynomes(Polynome[] tousPolys) {
	Polynome element = new Polynome();
	int i, j;
	
	for (i = 0; i < q; i++) {
	    for (j = 0; j < q; j++) {
		element = tousPolys[i].mult(tousPolys[j]);
		element = adjustePuissancesPolynome(element);
		System.out.print(element.affiche(m - 1) + " ");
	    }
	    System.out.print("\n");
	}		
    }          



    /** 
     * Divise deux polynomes dans Fp.
     * @param dividende  le polyn�me dividende
     * @param diviseur  le polyn�me diviseur
     * @return  un array contenant deux polyn�mes: le premier est le resultat
     * de la division, le deuxi�me le reste
     */
    public Polynome[] divise(Polynome dividende, Polynome diviseur) {
	int d1, d2;

	Polynome dividendeTemp = 
	                        new Polynome(adjusteCoeffsPolynome(dividende));
	Polynome resultat = new Polynome();

	while (dividendeTemp.degre() >= diviseur.degre()) {
	    d1 = dividendeTemp.degre();
	    d2 = diviseur.degre();
	    
	    // Adjuste le coefficient du premier terme du dividende 
	    // afin que le resultat de la division soit un nombre entier
	    while (dividendeTemp.coeff[d1] % diviseur.coeff[d2] != 0) {
		dividendeTemp.coeff[d1] += p;
	    }

	    resultat = resultat.addKXn(dividendeTemp.coeff[d1] / 
				       diviseur.coeff[d2], d1 - d2);
	    resultat = adjusteCoeffsPolynome(resultat);
	    dividendeTemp = dividendeTemp.add(diviseur.multKXn(
			        - dividendeTemp.coeff[d1] / diviseur.coeff[d2],
				d1 - d2));
	    dividendeTemp = adjusteCoeffsPolynome(dividendeTemp);
	}
	dividendeTemp = adjusteCoeffsPolynome(dividendeTemp);


	Polynome[] poly_output = new Polynome[2];
	poly_output[0] = new Polynome(resultat);        // resultat
	poly_output[1] = new Polynome(dividendeTemp);   // reste
	return poly_output;
    }



    /**
     * Construit la table des logarithmes de ce corps Fp, gener�e � partir 
     * du pr�mier �l�ment primitif trouv�.
     * @return  la table des logarithmes sous forme d'un array de polyn�mes,
     * o� l'�l�ment d'indice i contient (primitif^i)
     */
    private Polynome[] tableLogs() {
	int t;
	Polynome primitif = primitifs[0];
	Polynome[] puissancesPrimitif = new Polynome[q];

	for (t = 0; t < q; t++) {
	    puissancesPrimitif[t] = new Polynome();
	}

	for (t = 0; t <= q - 1; t++) {
	    puissancesPrimitif[t] = adjustePuissancesPolynome(primitif.pow(t));
	} 

	return puissancesPrimitif;
    }



    /**
     * Cherche dans la table des logarithmes le polynome primitif^ �gal au
     * polynome pass� en param�tre.
     * @param polynome  un polynome
     * @return  un entier i qui, dans la table des logarithmes, v�rifie 
     * primitif^i = polynome; -1 si un tel i n'existe pas (c'est le cas si 
     * polynome est nul)
     */
    public int reverseTableLogs(Polynome polynome) {
	for (int i = 0; i < tableLogs.length; i++) { 
	    if (tableLogs[i].equals(polynome)) {
		return i;
	    }
	}
	return -1;
    }



    /**
     * Recherche les elements primitifs de ce corps Fp.
     * @param tousPolys  tous les polyn�mes de Fp
     * @return  un array de polyn�mes contenant tous les elements primitifs 
     * de Fp 
     */
    private Polynome[] recherchePrimitifs(Polynome[] tousPolys) {
	Polynome element;
	int puissance;
	int i, n, k = 0;
	Polynome[] primitifs = new Polynome[q];

	// sont exclus du calcul le polynome nulle, le polynome 1
	// et toutes puissances q - 1

	prochain_poly: for (n = 2; n <= (q - 1); n++) {
	    // pour tout polynome de Fp, verifie s'il est primitif
	    element = new Polynome (tousPolys[n]);
	    for (i = q - 1; i >= 2; i--) {
		if ((q - 1) % i == 0) {
		    // l'ordre de l'element divise q - 1 => on teste
		    puissance = (q - 1) / i;
		    element = adjustePuissancesPolynome(
						 tousPolys[n].pow(puissance));
		    if (element.equals1()) {
			// une puissance du polynome donne 1 => pas primitif
			continue prochain_poly;		    
		    }
		}
	    }
	    primitifs[k++] = tousPolys[n];
	}
	return primitifs;
    }



    /**
     * Retourne l'invers d'un polynome dans Fp.
     * @param poly  un polyn�me
     * @return  le polyn�me inverse
     */
    public Polynome invers(Polynome poly) {
	Polynome generateur = primitifs[0];
	int n;	

	for (n = 1; n < q - 1; n++) {
	    if (poly.equals(adjustePuissancesPolynome(generateur.pow(n)))) 
		break;
	} 
	// on a trouve' n tel que le polynome en parametre = generateur^n
	
	return adjustePuissancesPolynome(generateur.pow(q - 1 - n));
    }
    
    

    /**
     * Cr�e le polyn�me minimal d'un �l�ment primitif alfa, � partir de la
     * classe cyclotomique de alfa.
     * @param  une liste d'entiers (p0=1, p1, p2, ...) qui sont les puissances
     * de la classe cyclotomique de alfa
     * @return  le polyn�me minimal (X - alfa)*(X - alfa^p1)*(X - alfa^p2)*...
     */
    public Polynome polynomeMinimal(Vector classeCyc) {
	Polynome minimal = new Polynome(1);
	for (int i = 0; i < classeCyc.size(); i++) {	    
	    Polynome pol = tableLogs[
			        ((Integer) classeCyc.elementAt(i)).intValue()];
	    pol = pol.multKXn(-1, 0);   // multiplie le polyn�me par -1
	    pol = pol.addKXn(1, 1);     // ajoute X au polyn�me
	    minimal = minimal.mult(pol);
	}

	Main.out("Classe cyclotomique: " + classeCyc);
	minimal = adjusteCoeffsPolynome(minimal);
	minimal = adjustePuissancesPolynome(minimal);
	if (minimal.degre() < classeCyc.size()) 
	    minimal = minimal.add(irreductible);
	Main.out("Polyn�me minimal associ�: " + minimal.toString());

	return minimal;
    }

    
}
