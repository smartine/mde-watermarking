package bchcodes;
import java.io.*;
import java.util.StringTokenizer;
import java.util.NoSuchElementException;


/**
 * Codes BCH (Bose - Chaudluri - Hocquenghem), 
 * classe de l'application principale.
 * Projet de Codage et Cryptographie, UMLV.
 * @author Daniele Raffo <draffo@etudiant.univ-mlv.fr> 
 * @version 1.2 - 11 FEV 2001
 */
public class Main {

    /**
     * Drapeau pour le mode "verbeux".
     */
    public static boolean verbose = false;



    /** 
     * Retourne le plus grand de deux entiers.
     * @param arg1  un entier
     * @param arg2  un entier
     * @return  le plus grand entre arg1 et arg2
     */
    public static int max(int arg1, int arg2) {
	if (arg1 > arg2) return arg1;
	else return arg2;
    }



    /** 
     * Verifie si un nombre est premier. (M�thode d'Eratostene).
     * @param nombre  un nombre entier
     * @return  true si nombre est premier, false sinon
     */
    public static boolean estPremier(int nombre) {
	int j, n, max = 100;
	boolean[] premiers = new boolean[max];

	for (j = 2; j < max; j ++)
	    premiers[j] = true;
	for (n = 2; n < max/2; n++) 
	    for (j = 2; n * j < max; j++) 
		premiers[n * j] = false;

	return (premiers[nombre]);
    }



    /**
     * Raccourci pour System.out.println(), mais affiche seulement si
     * la variable verbose est true.
     */
    public static void out(String str) {
	if (verbose) 
	    System.out.println(str);
    }



    /**
     * Raccourci pour System.out.print(), mais affiche seulement si
     * la variable verbose est true.
     */
    public static void outc(String str) {
	if (verbose) 
	    System.out.print(str);
    }



    /**
     * Raccourci pour System.out.println() avec deux all�es � la ligne, 
     * mais affiche seulement si la variable verbose est true.
     */
    public static void outl(String str) {
	if (verbose) {
	    System.out.println("");
	    System.out.println(str);
	}
    }
			


    public static void main(String args[]) throws IOException {

	System.out.println("\n================= Codes BCH =================");
	System.out.println(" Daniele Raffo <draffo@etudiant.univ-mlv.fr> ");
	System.out.println("      projet de Codage et Cryptographie");
	System.out.println("        Universit� de Marne la Vall�e");
	System.out.println("                 11 FEV 2001");
	System.out.println("=============================================\n");

	try {

	    if (args.length > 4) verbose = true;

	    if (args[0].equals("bch")) {

		int longueur = Integer.parseInt(args[1]);  // longueur du code
		int correct = Integer.parseInt(args[2]);   // code d-correcteur
		Polynome irred = new Polynome(args[3]);
		CodeBCH bch = new CodeBCH(longueur, correct, irred);

		System.out.println("\nG�n�ration du code BCH (longueur " + 
				   bch.n + ", dimension " + bch.dimensionBCH +
				   ", " + bch.d + "-correcteur) sur F2, \n" +
				   "polyn�me g�n�rateur G(X) = " +
				   bch.generateurBCH);
		System.out.println("On utilise le corps F" + bch.corps.q + 
				   " (2^" + bch.corps.m + 
				   "), polyn�me irreductible g�n�rateur " +
				   "P(X) = " + bch.corps.irreductible);

		while (true) {
		    System.out.print("\nBCH (" + bch.n + ", " + 
				     bch.dimensionBCH + ", " + bch.d + ")  " + 
				     "Commande > ");
		    BufferedReader br = new BufferedReader(
					     new InputStreamReader(System.in));
		    StringTokenizer st = new StringTokenizer(br.readLine());
		    try {
			String option = st.nextToken();

			if (option.equals("quit") || option.equals("q")) 
			    System.exit(0);

			if (!st.hasMoreTokens()) throw new CorpsException(
			        "Commandes : c[ode] <message � coder> \n" +
			        "            d[ecode] <message � decoder> \n" +
			        "            q[uit]");
			String strmsg = st.nextToken();
			StringBuffer gap = new StringBuffer("");
			for (int i = 1; i <= (bch.n - bch.dimensionBCH); i++)
			    gap.append(" ");

			if (option.equals("code") || option.equals("c")) {
			    if (strmsg.length() != bch.dimensionBCH)
				throw new CorpsException(
			              "Erreur: Ce message est de dimension " + 
				      strmsg.length() +
				      ", on attend un message � coder " +
				      "de dimension " + bch.dimensionBCH);
			    Polynome msg = bch.corps.adjusteCoeffsPolynome(
							 new Polynome(strmsg));
			    Polynome msgCode = bch.code(msg); 
			    System.out.println("Message     : " + 
				      msg.affiche(bch.dimensionBCH - 1) + gap +
				      "    " + msg);
			    System.out.println("Message cod�: " + 
		                msgCode.affiche(bch.n - 1) + "    " + msgCode);
			}

			else if (option.equals("decode") || 
				 option.equals("d")) { 
			    if (strmsg.length() != bch.n)
				throw new CorpsException(
				      "Erreur: Ce message est de dimension " +
				      strmsg.length() +
				      ", on attend un message � decoder " +
				      "de dimension " + bch.n);
			    Polynome msg = bch.corps.adjusteCoeffsPolynome(
						         new Polynome(strmsg));
			    Polynome msgDecode = bch.decode(msg);
			    String strm = msg.affiche(bch.n - 1);
			    String strmD = msgDecode.affiche(bch.n - 1);
			    StringBuffer strdiff = new StringBuffer("");
			    int errs = 0;
			    for (int ch = 0; ch < strm.length(); ch++) {
				if (strm.charAt(ch) != strmD.charAt(ch)) {
				    strdiff.append("^");  // un erreur detect�
				    errs++;  
				}
				else
				    strdiff.append(" ");
			    }
			    System.out.println("Message       : " + 
					       strm +  "    " + msg);
			    System.out.println("                " + 
					       strdiff + "    " + 
					       errs + " erreur(s) trouv�(s)");
			    System.out.println("Correction    : " +
					       strmD + "    " + msgDecode);
			    strmD = strmD.substring(0, bch.dimensionBCH); 
			    System.out.println("Message decod�: " + 
					       strmD + gap + "    " + 
					       new Polynome(strmD));
			}
			
		    } catch (CorpsException e) {

			System.out.println(e);

		    } catch (NoSuchElementException e2) {}
		}
	    }
	    
	    else if (args[0].equals("corps")) {

		int p = Integer.parseInt(args[1]);
		int m = Integer.parseInt(args[2]);  // corps Fp^m
		Polynome irreductible = new Polynome(args[3]);

		Corps corps = new Corps(p, m, irreductible);

		System.out.println("\nG�n�ration du corps fini F" + corps.q + 
			       " (" + corps.p + "^" + corps.m + 
			       "), polyn�me irreductible g�n�rateur P(X) = " + 
			       irreductible);

		System.out.println("\nTable de l'addition sur F" + corps.p);
		corps.afficheTableAdd();
		
		System.out.println("\nTable de la multiplication " + 
				   "sur F" + corps.p);
		corps.afficheTableMult();

		System.out.println("\nEnsemble de tous les polyn�mes " + 
				   "du corps");
		Polynome[] tlp = corps.tousLesPolynomes();
		for (int i = 0; i < corps.q; i++) {
		    System.out.println(tlp[i].affiche(m - 1) + "   " + tlp[i]);
		}

		System.out.println("\nTable de l'addition des polyn�mes");
		corps.afficheTableAddPolynomes(tlp);

		System.out.println("\nTable de la multiplication " + 
				   "des polyn�mes");
		corps.afficheTableMultPolynomes(tlp);

		System.out.println("\nElements primitifs");
		for (int i = 0; corps.primitifs[i] != null; i++) {
		    System.out.println(corps.primitifs[i]);
		}
	    
		System.out.println("\nTable des logarithmes");
		for (int i = 0; i < corps.q; i++) {
		    System.out.println("(" + corps.primitifs[0] + ")^" + i + 
				       " = " + corps.tableLogs[i]);
		}

		String reponse = new String("");
		Polynome poly = new Polynome();
		while (true) {
		    System.out.print("\nF" + corps.q + " (" + corps.p + "^" + 
				     corps.m + ")  Polyn�me � inverser > ");
		    BufferedReader br = new BufferedReader(
					     new InputStreamReader(System.in));
		    reponse = br.readLine();
		    if (reponse.equals("quit") || reponse.equals("q")) 
			System.exit(0);
		    if (reponse.equals("")) continue;
		    if (reponse.length() != corps.m) {
			System.out.println("Erreur: Entrer un polyn�me de " +
					   "degr� " + (corps.m - 1) + 
					   ", ou q[uit]");
			continue;
		    }
		    poly = corps.adjusteCoeffsPolynome(new Polynome(reponse));
		    System.out.println("Invers de " + poly + " =\n" + 
				       corps.invers(poly));	    
		}

	    } 

	    else if (args[0].equals("berlekamp")) {	

		if (args.length > 3) verbose = true;
		
		int p = Integer.parseInt(args[1]);
		Polynome poly = new Polynome(args[2]);
		Berlekamp berlekamp = new Berlekamp(p, poly);
	
	    } else {
		
		throw new ArrayIndexOutOfBoundsException();
		
	    } 
	    
	} catch (CorpsException e) {

	    System.out.println(e);

	} catch (ArrayIndexOutOfBoundsException e2) {  // pas assez d'arguments

	    System.out.println(
   "Utilisation:\n" +
   "java Main corps <p> <m> <polyn�me irr�ductible> [verbose]\n" +
   "      cr�e le corps fini Fq (q=p^m), ayant comme g�n�rateur\n" + 
   "      le polyn�me irreductible\n" +
   "java Main bch <n> <d> <polyn�me irr�ductible> [verbose]\n" +
   "      costruit un code BCH de longueur n sur F2, d-correcteur,\n" +
   "      en utilisant un corps fini g�n�r� par le polyn�me irreductible\n\n" +
   "Exemples:\n" +
   "java Main corps 2 4 10011    engendre F16 (2^4) par X^4 + X + 1\n" +
   "java Main bch 15 2 10011     construit un code BCH (15, x, 2)\n" +
   "                             sur un corps fini g�n�r� par  X^4 + X + 1");

	} finally {

	    System.out.println("");
	    System.exit(0);

	}
	
    }


}
