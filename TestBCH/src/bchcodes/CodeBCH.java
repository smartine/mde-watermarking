package bchcodes;
import java.util.Vector;


/**
 * Code correcteur d'erreur BCH (Bose - Chaudluri - Hocquenghem).
 */
public class CodeBCH {

    /**
     * Longueur du BCH (longueur finale des messages cod�s).
     */
    public int n;

    /**
     * Nombre d'erreurs que ce BCH peut corriger (d-correcteur).
     */
    int d;

    /**
     * Polyn�me g�nerateur de ce code BCH.
     */
    Polynome generateurBCH;

    /**
     * Dimension du BCH (c'est la longueur des messages � coder).
     */
    public int dimensionBCH;

    /**
     * Corps fini sur lequel est bas� ce code BCH.
     */
    public Corps corps;



    /**
     * Cr�e un code BCH de longueur n sur F2, d-correcteur.
     * @param irreductible  le polyn�me irreductible (� coefficient en Z/2Z, 
     * donc = {0, 1}) g�n�rateur du corps fini sur lequel est bas� ce code BCH
     */
    public CodeBCH(int n, int d, Polynome irreductible) throws CorpsException {

	this.n = n;
	this.d = d;
	if (n < 2 || n <= d) {
	    throw new CorpsException("Erreur: Longueur du code trop petite");
	}
	if (n % 2 == 0) {
	    throw new CorpsException("Erreur: BCH est sur F2 - la longueur " +
				     "du code doit �tre un nombre impair");
	}
	if (d < 1 || d > 3) {
	    throw new CorpsException("Seulement les codes BCH " +
				     "1-, 2- et 3-correcteur sont support�s");
	}

	// D�termine m, l'ordre de 2 modulo n
	int m = 1;
	while (Math.pow(2, m) % n != 1) m++;

	// On se place dans Fq (q = 2^m)
	corps = new Corps(2, m, irreductible);

	// R�cherche du polyn�me minimal pour chaque puissance du primitif
	// (il suffit de consid�rer les puissances impaires car la
	// caract�ristique du corps est 2)
	generateurBCH = new Polynome(1);
 	for (int puissance = 1; puissance < 2 * d; puissance += 2) {

	    // D�termine la classe cyclotomique 
	    Vector classeCyc = new Vector();
	    int elem = puissance;
	    classeCyc.addElement(new Integer(elem));
	    while (true) {
		elem = (elem * 2) % (corps.q - 1);
		if (classeCyc.contains(new Integer(elem))) break;
		classeCyc.addElement(new Integer(elem));
	    }

	    // Multiplies les polyn�me minimal relatif � cette 
	    // classe cyclotomique, par les autres polyn�mes minimaux
	    // dej� trouv�s. 
	    // On obtiendra ainsi � la fin le polyn�me g�nerateur du BCH
	    generateurBCH = generateurBCH.mult(
					    corps.polynomeMinimal(classeCyc));
	}

	generateurBCH = corps.adjusteCoeffsPolynome(generateurBCH);
	dimensionBCH = n - generateurBCH.degre();
	if (dimensionBCH < 1) {
	    throw new CorpsException("Erreur: Les donn�es de construction " +
				     "n'engendrent pas un code utilisable");
	}

    }



    /**
     * Code un message.
     * @param message  le message � coder, de longueur dimensionBCH
     * @return  le message cod�, de longueur n
     */
    public Polynome code(Polynome message) {
	Polynome messageCode = new Polynome(
				    message.multKXn(1, generateurBCH.degre()));
	Polynome[] co = corps.divise(messageCode, generateurBCH);
	Polynome controle = new Polynome(co[1]);

	return messageCode.add(controle);
    }



    /**
     * Decode un message. Il fait appel � la 
     * m�thode decode<d> pour la correction de d erreurs. 
     * @param messageCode  le message � d�coder
     * @return  le message decod�
     */
    public Polynome decode(Polynome messageCode) {
	Polynome[] syndromes = calculeSyndromes(messageCode);
	int erreurs[] = new int[5];

	switch (d) {
	case 1: erreurs = decode1(syndromes); break;
	case 2: erreurs = decode2(syndromes); break;
	case 3: erreurs = decode3(syndromes); break;
      //case 4: int[] erreurs decode4(syndromes); break;
      //case 5: int[] erreurs decode5(syndromes); break;
	}

	Polynome messageDecode = new Polynome(messageCode);

	// Corrige les erreurs en ajoutant des termes dans le message
	int positionErreur = 0;
	while (positionErreur < erreurs.length &&
	       erreurs[positionErreur] != -1) 
	    messageDecode = messageDecode.addKXn(1, erreurs[positionErreur++]);

	return corps.adjusteCoeffsPolynome(messageDecode);
    }



    /**
     * Calcule les 2 * d syndromes du message cod�.
     * @param messageCode  un message cod�, sous forme de polyn�me
     * @return  un array de 2 * d polyn�mes; chacun d'eux est un syndrome de
     * messageCode
     */   
    private Polynome[] calculeSyndromes(Polynome messageCode) {
	int nsyn = 2 * this.d;
	Main.outl("Calcul des " + nsyn + " syndromes de " + messageCode);

	Vector[] syn = new Vector[nsyn];  
	// Chaque �l�ment de syn[] contient la liste des puissances de alfa 
	// qui, additionn�es, composent le syndrome 
	Polynome[] syndromes = new Polynome[nsyn];
	// Chaque �l�ment contient un syndrome;
	// syndromes[s] est le polyn�me somme du contenu de syn[s]
	int[] idxsyn = new int[nsyn];
	// Chaque �l�ment est la puissance de alfa qui est �gale au syndrome;
	// idxsyn[s] = puissance ssi alfa^puissance = syndromes[s]

	for (int s = 1; s <= nsyn; s++) {
	    Main.outc("syndrome #" + s +":");

	    // Trouve les puissances de alfa qui composent le syndrome
	    syn[s-1] = new Vector();
	    for (int i = n - 1; i >= 0; i--) {
		if (messageCode.coeff[i] == 1) {
		    Main.outc(" + alfa^" + (i * s)); 
		    syn[s-1].addElement(new Integer(corps.adjuste(i * s)));
		}
	    }

	    // Additionne les alfa^ et construit le polynome syndrome
	    syndromes[s-1] = new Polynome();
	    for (int j = 0; j < syn[s-1].size(); j++) {
		// Additionne les puissances de alfa (trouv�es dans la 
		// table des logarithmes) pour obtenir le syndrome 
		syndromes[s-1] = syndromes[s-1].add(corps.tableLogs[
				((Integer) syn[s-1].elementAt(j)).intValue()
		                                                       ]);
	    }
	    syndromes[s-1] = corps.adjusteCoeffsPolynome(syndromes[s-1]);
	    Main.outc(" = " + syndromes[s-1].affiche(
                                              corps.irreductible.degre() - 1));

	    // Cherche dans la table des logarithmes la puissance de alfa
	    // qui corr�spond au syndrome trouv� (si ce dernier est non nul,
	    // la puissance existe bien)
	    idxsyn[s-1] = corps.reverseTableLogs(syndromes[s-1]);
	    if (idxsyn[s-1] != -1) Main.outc(" = alfa^" + idxsyn[s-1]);

	    Main.out("");
	}

	Main.out("");
	return syndromes;
    }



    /**
     * Corrige un erreur d'un message cod�, au moyen de calculs sur
     * les syndromes.
     * @param syndromes  les syndromes du message � d�coder
     * @return  un array d'entiers qui contient la position de l'erreur
     * dans le message  
     */ 
     public int[] decode1(Polynome[] syndromes) {
	Polynome alfa = new Polynome(corps.primitifs[0]);
	
	Polynome sigma1 = new Polynome(syndromes[0]);

	int a1 = corps.reverseTableLogs(sigma1);
	Main.outc("Polyn�me correcteur: X"); 
	if (a1 != -1) Main.outc(" + alfa^" + a1); 
	Main.out("");
	
	Main.outl("Recherche exaustive des solutions:");
	Polynome resultat = new Polynome();
	int[] erreurs = new int[this.d];
	int err = 0;
	for (int i = 0; i <= n; i++) {
	    resultat = alfa.pow(corps.adjuste(i));
	    if (a1 != -1)
		resultat = resultat.add(alfa.pow(a1));
	    resultat = corps.adjustePuissancesPolynome(resultat);
	    Main.outc("sigma(alfa^" + i + ") = " + 
		      resultat.affiche(corps.irreductible.degre() - 1));
	    if (resultat.equals0()) {
		Main.outc("   erreur en position " + i);
		erreurs[err++] = i;	
	    }
	    Main.out("");
	    if (err == this.d) break;
	}
	Main.out("");

	while (err < this.d) erreurs[err++] = -1;
	return erreurs;
    }



    /**
     * Corrige jusqu'� 2 erreurs d'un message cod�, au moyen de calculs sur
     * les syndromes.
     * @param syndromes  les syndromes du message � d�coder
     * @return  un array d'entiers qui contient les positions des erreurs
     * dans le message  
     */ 
    public int[] decode2(Polynome[] syndromes) {
	Polynome alfa = new Polynome(corps.primitifs[0]);
	
	Polynome sigma1 = new Polynome(syndromes[0]);
	Polynome sigma2 = new Polynome(corps.adjustePuissancesPolynome(
	                                    (syndromes[2].
	                                    add(syndromes[0].pow(3))).
                                            mult(corps.invers(syndromes[0]))));

	int a1 = corps.reverseTableLogs(sigma1);
	int a2 = corps.reverseTableLogs(sigma2);
	Main.outc("Polyn�me correcteur: X^2"); 
	if (a1 != -1) Main.outc(" + (alfa^" + a1 + ")X");
	if (a2 != -1) Main.outc(" + alfa^" + a2); 
	Main.out("");
	
	Main.outl("Recherche exaustive des solutions:");
	Polynome resultat = new Polynome();
	int[] erreurs = new int[this.d];
	int err = 0;
	for (int i = 0; i <= n; i++) {
	    resultat = alfa.pow(corps.adjuste(i * 2));
	    if (a1 != -1) 
		resultat = resultat.add(alfa.pow(corps.adjuste(i + a1)));
	    if (a2 != -1)
		resultat = resultat.add(alfa.pow(a2));
	    resultat = corps.adjustePuissancesPolynome(resultat);
	    Main.outc("sigma(alfa^" + i + ") = " + 
		      resultat.affiche(corps.irreductible.degre() - 1));
	    if (resultat.equals0()) {
		Main.outc("   erreur en position " + i);
		erreurs[err++] = i;	
	    }
	    Main.out("");
	    if (err == this.d) break;
	}
	Main.out("");

	while (err < this.d) erreurs[err++] = -1;
	return erreurs;
    }


    /**
     * Corrige jusqu'� 3 erreurs d'un message cod�, au moyen de calculs sur
     * les syndromes.
     * @param syndromes  les syndromes du message � d�coder
     * @return  un array d'entiers qui contient les positions des erreurs
     * dans le message  
     */ 
     public int[] decode3(Polynome[] syndromes) {
	Polynome alfa = new Polynome(corps.primitifs[0]);

	/*** FIX ME ***/
	
	Polynome sigma1 = new Polynome(syndromes[0]);
	Polynome sigma2 = new Polynome(corps.adjustePuissancesPolynome(
	         (syndromes[4].add((syndromes[0].pow(2)).mult(syndromes[2]))).
	         mult(corps.invers((syndromes[0].pow(3)).add(syndromes[2])))));
	Polynome sigma3 = new Polynome(corps.adjustePuissancesPolynome(
					      (syndromes[0].pow(3)).
					      add(syndromes[2]).
					      add(syndromes[0].mult(sigma2))));

	int a1 = corps.reverseTableLogs(sigma1);
	int a2 = corps.reverseTableLogs(sigma2);
	int a3 = corps.reverseTableLogs(sigma3);
	Main.outc("Polyn�me correcteur: X^3"); 
	if (a1 != -1) Main.outc(" + (alfa^" + a1 + ")X^2");
	if (a2 != -1) Main.outc(" + (alfa^" + a2 + ")X"); 
	if (a3 != -1) Main.outc(" + alfa^" + a3); 
	Main.out("");
	
	Main.outl("Recherche exaustive des solutions:");
	Polynome resultat = new Polynome();
	int[] erreurs = new int[this.d];
	int err = 0;
	for (int i = 0; i <= n; i++) {
	    resultat = alfa.pow(corps.adjuste(i * 3));
	    if (a1 != -1) 
		resultat = resultat.add(alfa.pow(corps.adjuste(i * 2 + a1)));
	    if (a2 != -1)
		resultat = resultat.add(alfa.pow(corps.adjuste(i + a2)));
	    if (a3 != -1)
		resultat = resultat.add(alfa.pow(a3));
	    resultat = corps.adjustePuissancesPolynome(resultat);
	    Main.outc("sigma(alfa^" + i + ") = " + 
		      resultat.affiche(corps.irreductible.degre() - 1));
	    if (resultat.equals0()) {
		Main.outc("   erreur en position " + i);
		erreurs[err++] = i;	
	    }
	    Main.out("");
	    if (err == this.d) break;
	}
	Main.out("");

	while (err < this.d) erreurs[err++] = -1;
	return erreurs;
    }


}
