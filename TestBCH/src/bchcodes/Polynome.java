package bchcodes;
/**
 * Classe de d�finition d'un polyn�me � coefficients entiers.
 */
public class Polynome {

    /**
     * Degr� maximal d'un polyn�me.
     */
    static final int MAX_DEGRE = 128;

    /**
     * Les coefficients du polyn�me. L'�l�ment [n] de l'array est la
     * valeur du coefficient de X^n.
     */   
    int[] coeff = new int[MAX_DEGRE];


    /**
     * Construit un polyn�me dont les coefficients sont les valeurs stock�s 
     * dans l'array, dans le m�me ordre.
     * @param coeffArg  l'array contenant les coefficients du polyn�me.
     * Les valeurs contenus sont, dans l'ordre, les coefficients de
     * X^n, X^(n-1), ..., X^1, X^0. Remarquer que l'ordre est invers� 
     * par rapport au champ Polynome.coeff  
     */    
    public Polynome(int[] coeffArg) { 
	int deg = coeffArg.length - 1;
	for (int i = 0; i <= deg; i++) {  
	    this.coeff[i] = coeffArg[deg - i];
	}
    }


    /**
     * Construit un polyn�me dont les coefficients sont les valeurs pass�s
     * en argument, dans le m�me ordre.
     * @param coeffsArg  une String contenant les coefficient du polyn�mes,
     * sans espaces entre eux. 
     * Les valeurs contenus sont, dans l'ordre, les coefficients de
     * X^n, X^(n-1), ..., X^1, X^0. Remarquer que l'ordre est invers� 
     * par rapport au champ Polynome.coeff  
     */    
    public Polynome(String coeffsArg) { 
	int[] array = new int[coeffsArg.length()];
	for (int i = 0; i < coeffsArg.length(); i++) {
	    try {
		array[i] = Integer.parseInt(coeffsArg.substring(i, i+1));
	    } catch (NumberFormatException e) {
		array[i] = 0;
	    }
	}
	for (int i = 0; i <= coeffsArg.length() - 1; i++) {  
	    this.coeff[i] = array[coeffsArg.length() - 1 - i];
	}
    }


    /**
     * Construit un polyn�me �gal au polyn�me pass� en argument.
     * @param polyArg  le polyn�me qu'on veut cloner
     */
    public Polynome(Polynome polyArg) {
	for (int i = 0; i <= polyArg.degre(); i++) {
	    this.coeff[i] = polyArg.coeff[i];
	}
    }	


    /**
     * Construit le polyn�me P(X) = constante.
     * @param constante  un nombre entier
     */
    public Polynome(int constante) {
	this.coeff[0] = constante;
	for (int i = 1; i <= MAX_DEGRE - 1; i++) {
	    this.coeff[i] = 0;
	}
    }


    /**
     * Construit le polyn�me nul, c'est � dire P(X) = 0.
     */
    public Polynome() {
	this(0);
    }



     /**
      * Calcules le degr� d'un polyn�me. (C'est la puissance la plus grande 
      * de X � coefficient non nul)
      * @return  le degr� du polyn�me 
      */
    public int degre() {
	for (int i = MAX_DEGRE - 1; i >= 0; i--) {
	    if (coeff[i] != 0) return i;
	}	

	return 0;                                
    }



    /**
     * Ajoute K*X^n a' un polyn�me.
     * @param k  un entier
     * @param n  un entier 
     * @return  le polyn�me this + K*X^n 
     */
    public Polynome addKXn(int k, int n) {
	Polynome poly = new Polynome(this);	

	poly.coeff[n] += k;

	return poly;
    }
    

    
    /** 
     * Somme deux polyn�mes.
     * @param poly  un polyn�me
     * @return  le polyn�me this + poly  
     */
    public Polynome add(Polynome poly) {
	Polynome polySomme = new Polynome();
	int degre = Main.max(degre(), poly.degre());

	for (int i = 0; i <= degre; i++) {
	    polySomme.coeff[i] = coeff[i] + poly.coeff[i];
	}		 	

	return polySomme;
    }



    /**
     * Multiplies un polyn�me par K*X^n.
     * @param k  un entier
     * @param n  un entier 
     * @return  le polyn�me this, multipli� par K*X^n
     */ 
    public Polynome multKXn(int k, int n) {
	Polynome poly = new Polynome();

	for (int i = degre(); i >= 0; i--) {
	    poly.coeff[i + n] = coeff[i] * k;
	}	

	return poly;
    }
   
 

    /**
     * Multiplies deux polynomes.
     * @param poly  un polyn�me
     * @return  le polyn�me this * poly  
     */
    public Polynome mult(Polynome poly) {
	Polynome polyProduit = new Polynome();
	Polynome polyTmp = new Polynome();
	
	for (int i = poly.degre(); i >= 0; i--) {
	    polyTmp = this.multKXn(poly.coeff[i], i);
	    polyProduit = polyProduit.add(polyTmp);
	}
	
	return polyProduit;
    }
 
   

    /**
     * El�ve un polyn�me � la puissance g.
     * @param g  un entier
     * @return  le polyn�me this^g
     */
    public Polynome pow(int g) {
	Polynome polyPuissance = new Polynome(this);

	if (g == 0) return new Polynome(1);
	if (g == 1) return polyPuissance;
	for (int i = 2; i <= g; i++) {
	    polyPuissance = polyPuissance.mult(this);
	}

	return polyPuissance;
    }



    /** 
     * Teste si un polynome est nul.
     * @return  true si this est P(X) = 0, false sinon
     */
    public boolean equals0() {
	return ((degre() == 0) && (coeff[0] == 0));
    }



    /** 
     * Teste si un polynome est P(x) = 1. 
     * @return  true si this est P(X) = 1, false sinon
     */
    public boolean equals1() {
	return ((degre() == 0) && (coeff[0] == 1));
    }



    /**
     * Teste l'egalite' de deux polyn�mes
     * @param poly  un polyn�me
     * @return  true si this et poly ont co�fficients �gaux, false sinon
     */
    public boolean equals(Polynome poly) {

	if (degre() != poly.degre()) return false;
	for (int i = 0; i <= degre(); i++) {
	    if (coeff[i] != poly.coeff[i]) return false;
	}

	return true;
    }



    /** 
     * Affiche un polyn�me.
     * @return  une r�presentation du polyn�me, pour exemple 
     * 3X^7 8X^6 X^5 2X^2 5X 1 
     */
    public String toString() { 
	if ((degre() == 0) && (coeff[0] == 0)) return "0";	
	StringBuffer buff = new StringBuffer();
	for (int i = degre(); i >= 0; i--) {
	    if (coeff[i] != 0) {
		if ((coeff[i] != 1) || (i == 0)) buff.append(coeff[i]);
		if (i != 0) buff.append("X");
		if (i > 1) buff.append("^" + i);
		buff.append(" ");
	    }
	}
	return buff.toString();
    }
    


    /**
     * Affiche un polyn�me comme une simple suite de ses coefficients,
     * � partir du coefficient de X^degreMax jusqu'au coefficient de X^0,
     * sans espaces s�paratoires.
     * Ceci est tr�s pratique quand on est en base2.
     * @param degreMax  le degr� maximale de la X � partir duquel on veut
     * afficher les coefficients. Seront affich�s donc (degreMax + 1) 
     * coefficients
     * @return la liste des coefficients du polynome, pour exemple 0010111 
     */
    public String affiche(int degreMax) {
	StringBuffer buff = new StringBuffer();
	
	for (int i = degreMax; i >= 0; i--) {
	    buff.append(coeff[i]);
	}
	
	return buff.toString();
    }
    
    
}
