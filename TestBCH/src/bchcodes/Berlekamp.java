package bchcodes;
/**
 * Algorithme de Berlekamp pour la d�composition des polyn�mes.
 */
public class Berlekamp {

    /**
     * Les polyn�mes irreductibles, resultat de la decomposition.
     */
    static private Polynome[] facteurs;

    static private int facteurIndex;

    Corps corps;

    int p;


    /**
     * Constructeur pour la d�composition d'un polynome dans Fp.
     */
    public Berlekamp(int p, Polynome polynome) throws CorpsException {

	Polynome polyDebut = new Polynome(polynome);
	Main.outl("D�composition de " + polyDebut + " dans F" + p + 
		  " selon Berlekamp");

	// V�rifie si on peut "sortir" la caract�ristique
	boolean sortir = true;
	for (int i = 0; i <= polyDebut.degre(); i++) {
	    if ((polyDebut.coeff[i] != 0) && (i % p != 0)) sortir = false;
	}
	if (sortir) {
	    // Fait "sortir" la caracteristique
	    Polynome tmp = new Polynome(0);
	    Polynome poly1 = new Polynome(1);
	    for (int j = 0; j <= polyDebut.degre(); j++) {
		if ((j % p) == 0) { 
		    tmp = tmp.add(poly1.multKXn(polyDebut.coeff[j], 
						(j / p)));
		}
	    }
	    polyDebut = new Polynome(tmp);
	    Main.out("Le polyn�me a et� simplifi� en " + polyDebut);
	}

	// Cr�e un corps pour la d�composition. m sera utilis� pour 
	// engendrer tous les combinaisons lin�aires des �l�ments de Fp, 
	// qui seront utilis�es pour la matrice B - Id; polyDebut
	// sera utilis� pour simplifier les x^kp
	corps = new Corps(p, polyDebut);
	this.p = p;

	factorise(polyDebut);
    }



    /**
     * Initialise la structure de donn�es pour les facteurs irr�ductibles.
     */
    public static void initialise() {
	facteurs = new Polynome[200];
	for (int i = 0; i < 200; i++)
	    facteurs[i] = new Polynome();
	facteurIndex = 0;
    }



    /**
     * Factorise poly.
     * Si poly n'est pas factorisable (irr�ductible), il est stock� dans
     * l'array facteurs, sinon on factorise poly en deux polynomes et on
     * appelle r�cursivement cette m�thode sur chacun d'eux.
     */
    void factorise(Polynome poly) {

	Polynome[] gen = new Polynome[poly.degre()];

	// Engendre tous les x^0, x^p, x^2p, x^3p ... x^kp .
	// Les x^kp avec kp >= au degr� de poly vont �tre simplifi�s 
	Main.outl("G�n�rateurs de la factorisation:");
	for (int k = 0; k < poly.degre(); k++) {
	    gen[k] = new Polynome(1).multKXn(1, k*p);
	    if (k*p >= poly.degre()) 
		gen[k] = corps.adjustePuissancesPolynome(gen[k]);
	    Main.out("x^" + (k*p) + " = " + gen[k]);
	}

	// Cr�e les matrices B et B-Id
	int[][] matB = new int[poly.degre()][poly.degre()]; 

	/*** TO DO ***/


    }




}
