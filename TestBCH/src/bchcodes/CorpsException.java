package bchcodes;
/**
 * Exception lanc�e en cas d'erreur pendant les operations sur un corps fini.
 */
public class CorpsException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String d;
    
    CorpsException(String d) {
	this.d = d;
    }

    CorpsException() {
	this.d = "";
    }



    public String toString() {
	if (d.equals("")) return "";
	else return (d);
    }

}
