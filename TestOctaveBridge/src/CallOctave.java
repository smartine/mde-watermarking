


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ProcessBuilder.Redirect;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class CallOctave {


	public static void main(String[] args) throws IOException {
		
		Integer[] result = new Integer[0];
		
		CallOctave CO = new CallOctave();
		try {
			result = CO.callOctaveBch(7, 2, new int[] {0,1,3,3,1,2,6,6,3,5,1,0,1,0,3});
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		System.out.printf("Process terminated with:");
		System.out.println(Arrays.toString(result));
		
	}

	public Integer [] getArray(String line) {
		LinkedList<Integer> numbers = new LinkedList<Integer>();

		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher(line); 
		while (m.find()) {
			numbers.add(Integer.parseInt(m.group()));
		}
		return numbers.toArray(new Integer[numbers.size()]);
	}

	public Integer[] callOctaveBch(int k, int t, int[] bytes) throws IOException, InterruptedException {
		Integer[] result = new Integer[0];

		String[] cmd = {"cmd.exe", "/C", "C:\\Users\\sm251494\\Octave\\octave-4.0.1\\bin\\octave-cli.exe","--eval","bchdeco ("+ Arrays.toString(bytes) +", "+ k +", "+ t +")"};

		List<String> lines = callExec(cmd);
		
		for (String line : lines) {
			Integer[] lineresult = getArray(line);
			if (lineresult.length > 0) {
				result = lineresult;
			}
		}

		return result;
	}

	public List<String> callExec(String [] cmd) throws IOException, InterruptedException {
		LinkedList<String> returnLines = new LinkedList<String>();
		ProcessBuilder pb = new ProcessBuilder(cmd);
		Process proc;

		proc = pb.start();


		// handle each of proc's streams in a separate thread
		ExecutorService handlerThreadPool = Executors.newFixedThreadPool(1);
		
//        handlerThreadPool.submit(new Runnable() {
	//	            @Override
	//	            public void run() {
	//	                // we want to write to the stdin of the process
	//	                BufferedWriter stdin = new BufferedWriter(
	//	                    new OutputStreamWriter(proc.getOutputStream()));
	//
	//	                // read from our own stdin so we can write it to proc's stdin
	//	                BufferedReader myStdin = 
	//	                new BufferedReader(new InputStreamReader(System.in));
	//	                String line = null;
	//	                try {
	//	                    do {
	//	                        line = myStdin.readLine();
	//	                        stdin.write(String.format("%s%n", line));
	//	                        //stdin.flush();
	//	                        stdin.close();
	//	                    } while(! "exit".equalsIgnoreCase(line));
	//	                } catch(IOException e) {
	//	                    e.printStackTrace();
	//	                }
	//	            }
	//	        });
		
//        handlerThreadPool.submit(new Runnable() {
	//	            @Override
	//	            public void run() {
	//	                // we want to read the stderr of the process
	//	                BufferedReader stderr = new BufferedReader(
	//	                    new InputStreamReader(proc.getErrorStream()));
	//	                String line;
	//	                try {
	//	                    while(null != (line = stderr.readLine())) {
	//	                        System.err.printf("[stderr] %s%n", line);
	//	                    }
	//	                } catch(IOException e) {
	//	                    e.printStackTrace();
	//	                }
	//	            }
	//	        });

	// wait for the process to terminate

		handlerThreadPool.submit(new Runnable() {
			@Override
			public void run() {
				// we want to read the stdout of the process
				BufferedReader stdout = new BufferedReader(
						new InputStreamReader(proc.getInputStream()));
				String line;
				try {
					while(null != (line = stdout.readLine())) {
						returnLines.add(line);      
					}
				} catch(IOException e) {
					e.printStackTrace();
				}
			}
		});

		int exitCode = 0;
		exitCode = proc.waitFor();
		handlerThreadPool.shutdown();
		return returnLines;
	}

}

	
	
	

	

