package org.modelversioning.ecoremutator.mutations.ecore.impl;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.modelversioning.ecoremutator.IModelProvider;
import org.modelversioning.ecoremutator.mutations.AbstractMutation;
import org.modelversioning.ecoremutator.tracker.IMutationTracker;

public class RemoveEClassMutation extends AbstractMutation{

	@Override
	public String getId() {
		return "mutation.ecore.removeEClass";
	}

	@Override
	public boolean canHandleEditingDomain() {
		return false;
	}

	@Override
	public boolean mutate(IModelProvider modelProvider, IMutationTracker tracker) {
		EObject eClassObject = modelProvider
				.getRandomEObject(EcorePackage.eINSTANCE.getEClass());
		if (eClassObject != null && eClassObject instanceof EClass) {
			EClass eClass = (EClass) eClassObject;
			System.out.println("Removing and Has attributes" + eClassObject.toString());
			EcoreUtil.delete(eClassObject, false);
			return true;
		} else {
			log(IStatus.WARNING,
					"Model provider did not return a suitable object to delete.");
			tracker
					.track(
							this.getId(),
							"Model provider did not return a suitable object to delete.",
							false, toEObjectList(null), toFeatureList(null));
		}
		
		return false;
	}

}
