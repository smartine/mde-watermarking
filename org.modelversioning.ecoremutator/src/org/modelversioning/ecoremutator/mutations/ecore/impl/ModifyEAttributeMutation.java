package org.modelversioning.ecoremutator.mutations.ecore.impl;

import java.util.Random;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.modelversioning.ecoremutator.IModelProvider;
import org.modelversioning.ecoremutator.mutations.AbstractMutation;
import org.modelversioning.ecoremutator.tracker.IMutationTracker;

public class ModifyEAttributeMutation extends AbstractMutation{
	
	/**
	 * {@inheritDoc}
	 * 
	 * Adds a random {@link EClass}.
	 */
	@Override
	public boolean mutate(IModelProvider modelProvider, IMutationTracker tracker) {
		System.out.println("Modifiying");
		boolean success = false;
		Exception occurredException = null;
		
		Random generator = new Random(12345);

		EObject eClassObject = modelProvider
				.getRandomEObject(EcorePackage.eINSTANCE.getEClass());
		System.out.println("Modifiying" + eClassObject.toString());
		if (eClassObject != null && eClassObject instanceof EClass) {
			EClass eClass = (EClass) eClassObject;
			if (eClass.getEAttributes().size() > 0) {
				System.out.println("Modifiying and Has attributes" + eClassObject.toString());
				EAttribute ef = eClass.getEAttributes().get(generator.nextInt(eClass.getEAttributes().size()));
				ef.setName(ef.getName().concat("AB"));
				success = true;
				String message = "Modified EAttribute " + ef.getName()
					+ " and added it to " + eClass.getName();
			}else {
				success = false;
			}
		} else {
			success = false;
			String message = "Could modify reference.";
			log(IStatus.INFO, message);
			log(IStatus.WARNING, message, occurredException);
			tracker.track(this.getId(), message, false, toEObjectList(null),
					toFeatureList(null));
		}

		return success;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getId() {
		return "mutation.ecore.ModifyEAttribute";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean canHandleEditingDomain() {
		return false;
	}

}
