package org.cea.lise.security.watermarking.mutation;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.modelversioning.ecoremutator.EcoreMutator;
import org.modelversioning.ecoremutator.EcoreMutatorPlugin;
import org.modelversioning.ecoremutator.IModelProvider;
import org.modelversioning.ecoremutator.mutations.ModelProvider;
import org.modelversioning.ecoremutator.mutations.ecore.impl.AddEAttributeMutation;
import org.modelversioning.ecoremutator.mutations.ecore.impl.AddEClassMutation;
import org.modelversioning.ecoremutator.mutations.ecore.impl.AddEReferenceMutation;
import org.modelversioning.ecoremutator.mutations.ecore.impl.AddSuperTypeMutation;
import org.modelversioning.ecoremutator.mutations.ecore.impl.ModifyEAttributeMutation;
import org.modelversioning.ecoremutator.mutations.ecore.impl.RemoveEClassMutation;
import org.modelversioning.ecoremutator.mutations.impl.AddAnnotationMutation;
import org.modelversioning.ecoremutator.mutations.impl.AddObjectMutation;
import org.modelversioning.ecoremutator.mutations.impl.DeleteObjectMutation;
import org.modelversioning.ecoremutator.mutations.impl.MoveObjectMutation;
import org.modelversioning.ecoremutator.mutations.impl.UnsetFeatureMutation;
import org.modelversioning.ecoremutator.mutations.impl.UpdateFeatureMutation;

public class StandardMutateEMF {

	public static void main(String[] args) {
		
		
		EcoreMutator mutator = new EcoreMutator();
		

		// configure mutations to apply
		mutator.addMutation(new AddEAttributeMutation());
		mutator.addMutation(new ModifyEAttributeMutation());
		mutator.addMutation(new AddEClassMutation());
		mutator.addMutation(new AddEReferenceMutation());
		mutator.addMutation(new AddSuperTypeMutation());
		mutator.addMutation(new RemoveEClassMutation());

		// configure a change tracker
		//mutator.setTracker(null);

		// load input model to mutate (any ecore-based model resource)
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
        Map<String, Object> m = reg.getExtensionToFactoryMap();
        m.put("ecore", new XMIResourceFactoryImpl());
        m.put("xmi", new XMIResourceFactoryImpl());
        ResourceSet resSet = new ResourceSetImpl();
        
		// initialize model provider
		IModelProvider modelProvider = new ModelProvider();
		Resource metamodel = resSet.getResource(URI.createURI("model" + File.separator + "Matlab.ecore"), true);
		Resource inputResource = resSet.getResource(URI.createURI("model" + File.separator + "Matlab.ecore"), true);
		modelProvider.setModelResource(inputResource);
		
		try {
			inputResource.load(m);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// mutate model
		mutator.mutate(modelProvider, 50);
		
		// save output model
		URI output = URI.createURI("model" + File.separator + "Matlab_50.ecore");
		Resource outputResource = resSet.createResource(output);
		outputResource.getContents().addAll(inputResource.getContents());
		try {
			outputResource.save(Collections.emptyMap());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
